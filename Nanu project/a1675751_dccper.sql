
-- phpMyAdmin SQL Dump
-- version 2.11.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 22, 2015 at 08:26 AM
-- Server version: 5.1.57
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `a1675751_dccper`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(75) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `photo` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` VALUES(4, 'Chris H', 'chris.jpg');
INSERT INTO `member` VALUES(5, 'Hannah S', 'hannah.jpg');
INSERT INTO `member` VALUES(6, 'Joshua O', 'joshua.jpg');
INSERT INTO `member` VALUES(7, 'Luke K', 'luke.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ta`
--

CREATE TABLE `ta` (
  `id` int(75) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `task` text NOT NULL,
  `statrd` text NOT NULL,
  `endd` text NOT NULL,
  `estimate` text NOT NULL,
  `spent` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `ta`
--

INSERT INTO `ta` VALUES(1, 'Chris H', 'create login page', '12/12/2015', '13/12/2015', '12', '19');
INSERT INTO `ta` VALUES(2, 'Chris H', 'design home page', '14/12/2015', '14/12/2015', '6', '2');
INSERT INTO `ta` VALUES(3, 'Chris H', 'create contact page', '15/12/2015', '16/12/2015', '20', '9.5');
INSERT INTO `ta` VALUES(4, 'Chris H', 'trouble shoot contact page', '17/12/2015', '18/12/2015', '15', '10');
INSERT INTO `ta` VALUES(5, 'Chris H', 'create registration of user', '19/12/2015', '20/12/2015', '14', '4');
INSERT INTO `ta` VALUES(6, 'Chris H', 'create admin control panel', '22/12/2015', '23/12/2015', '9', '1.8');
INSERT INTO `ta` VALUES(7, 'Chris H', 'troubleshoot admin control panel', '24/12/2015', '29/12/2015', '32', '12');
INSERT INTO `ta` VALUES(8, 'Hannah S', 'Admin Pannel', '02/12/2015', '03/12/2015', '10', '3');
INSERT INTO `ta` VALUES(9, 'Hannah S', 'evaluating project database', '02/01/2016', '03/01/2016', '20', '16');
INSERT INTO `ta` VALUES(10, 'Hannah S', 'testing reports of sports club', '06/01/2016', '06/01/2016', '6', '9');
INSERT INTO `ta` VALUES(11, 'Luke K', 'writing test cases ', '04/01/2016', '06/01/2016', '25', '29');
INSERT INTO `ta` VALUES(12, 'Joshua O', 'designing mobile app for sunshine limited', '02/01/2016', '05/01/2016', '28', '25');
INSERT INTO `ta` VALUES(13, 'Hannah S', 'deploying the updated website', '06/01/2016', '06/01/2016', '3', '2');
INSERT INTO `ta` VALUES(14, 'Hannah S', 'debugging user control panel', '07/01/2016', '07/01/2016', '6', '9');
